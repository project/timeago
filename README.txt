Description:
------------
Yet another module. This one prints out the the amount of time ago a node is written.


Usage:
------
use the variable $timeago in your node.tpl.php file.
You can configure this variable in your settings.


Author
------
Jonas Wouters (http://drupal.org/user/17) http://zertox.com